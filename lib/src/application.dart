import 'dart:async';
import './interface.dart';
import './services.dart';
import './system.dart';


class Application {
  List<Interface> interfaces;
  Map<String,dynamic> config;

  Application({this.interfaces,Map<String,Service> services}){
    if (interfaces == null)
      this.interfaces = [];
    new RingAccpeter((Ring ring) => _startInterfaces()).accept('system.is_started');
  }

  void start(){
    system.startup();
  }

  void _startInterfaces(){
    interfaces.forEach((Interface interface){
      interface.init(this);
    });
  }
}

