import './application.dart' show Application;
import 'dart:async';

abstract class Interface {
  Future init(Application application);
}

