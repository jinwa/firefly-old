import 'dart:mirrors';
import 'dart:async';
import 'dart:convert';

// Copy from dart:core
class AssertionError extends Error {
  /** Message describing the assertion error. */
  final Object message;
  AssertionError([this.message]);
  String toString() => "Assertion failed"+_getMoreInfo();

  String _getMoreInfo(){
    if (message != null){
      return ": $message";
    } else {
      return ".";
    }
  }
}


class TestCase {
  void check(bool expresion, [Object whenFail]){
    if (!expresion){
      throw new AssertionError(whenFail);
    }
  }

  void checkHasKey(Map map, dynamic target, [Object whenFail]){
    check(map.containsKey(target),whenFail);
  }

  void checkHasKeyValue(Map map, dynamic k,dynamic v, [Object whenFail]){
    check(map[k] == v,whenFail);
  }

  void checkHasValue(List list,dynamic value, [Object whenFail]){
    check(list.contains(value),whenFail);
  }

  void fail([String message]){
    check(false,message ?? "Test run .fail(${message ?? ''})");
  }
}


final List<TestCase> _testCases = [];
final List<ErrTrace> _errors = [];


class ErrTrace {
  String location;
  dynamic error;
  StackTrace trace;

  ErrTrace(this.location,this.error,this.trace);
}


const PNOTICE = '!';
const PASK = '?';
const PINFO = '.';
const PERR = 'E';


void tprint(String level, String string){
  print("[$level] $string");
}


void registerCase(Type caseType) {
  var mirror = reflectType(caseType);
  if (!mirror.isSubtypeOf(reflectType(TestCase))){
    tprint(PNOTICE, "Case ${mirror.simpleName} is not registered! The class' type must be a subtype of TestCase");
    return;
  }
  _testCases.add(reflectClass(caseType).newInstance(new Symbol(''), []).reflectee);
}


void runTest(){
  int runned = 0;
  _testCases.forEach((tcase){
    try{
      runned += _runCase(tcase);
    } catch(e,st){
      tprint(PERR, "Unhandled Excepiton\n$e\n$st");
    }
  });
  if (_errors.length == 0){
    tprint(PINFO, "[$runned|0] Test done! There is not any errors. $runned tests had been ran");
  } else {
    tprint(PINFO, "[$runned|${_errors.length}] There were errors found.");
    _errors.forEach((e) => tprint(PERR,"${e.location}\n${e.error}\n${e.trace}"));
  }
}


Map<K,V> _mapfilter<K,V>(Map<K,V> map,bool f(K key,V value)){
  var newMap = {};
  map.forEach((k,v){
    if (f(k,v)){
      newMap[k] = v;
    }
  });
  return newMap;
}

String sym2str(Symbol symbol){
  return MirrorSystem.getName(symbol);
}


int _runCase(TestCase tcase){
  var ins = reflect(tcase);
  var name = sym2str(ins.type.simpleName);
  var fsetUpAll = ins.getField(new Symbol('setUpAll'));
  if (fsetUpAll.hasReflectee){
    fsetUpAll.reflectee();
  }
  var tests = _mapfilter<Symbol,MethodMirror>(
    ins.type.instanceMembers,
    (symbol,m) {
      var name = sym2str(symbol);
      var result = name.toLowerCase().startsWith("test");
      return result;
    }
  );
  var fsetUp = ins.getField(new Symbol('setUp'));
  var hasSetUp = fsetUp.hasReflectee;
  var ftearDown = ins.getField(new Symbol('tearDown'));
  var hasTearDown = ftearDown.hasReflectee;
  var curr = 0;
  tests.forEach((symbol,mirror){
    var symbolName = sym2str(symbol);
    tprint(PINFO, "Turn to $name.$symbolName [${curr+1}/${tests.length}]");
    try {
      if (hasSetUp){
        var future = fsetUp.reflectee();
        if (future != null && future is Future){
          Future.wait([future]);
        }
      }
    } catch(e,st){
      tprint(PERR, "setUp step has been error before $name.$symbolName");
      tprint(PERR, "$e");
      tprint(PERR, "$st");
      _errors.add(new ErrTrace("$name.$symbolName",e,st));
      return;
    }
    try {
      ++curr;
      var future = ins.invoke(mirror.simpleName, []);
      if (future.hasReflectee && future.reflectee is Future){
        Future.wait([future]);
      }
    } catch(e,st) {
      tprint(PERR, "$name.$symbolName has been error");
      _errors.add(new ErrTrace("$name.$symbolName",e,st));
    }
    try {
      if (hasTearDown){
        var future = ftearDown.reflectee();
        if (future != null && future is Future){
          Future.wait([future]);
        }
      }
    } catch(e,st){
      tprint(PERR, "tearDown step has been error after $name.$symbolName");
      tprint(PERR, "$e");
      tprint(PERR, "$st");
      _errors.add(new ErrTrace("$name.$symbol",e,st));
      return;
    }
  });
  var ftearDownAll = ins.getField(new Symbol('tearDownAll'));
  if (ftearDownAll.hasReflectee){
    ftearDownAll.reflectee();
  }
  return curr;
}
