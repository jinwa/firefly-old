import 'dart:async';

abstract class Service {
  Future init(ServiceController controller);
}


class ServiceController {
  bool isServiceStarted = false;
  ServiceHost _host;
  String name;
  Service service;

  ServiceController(this._host,this.name,this.service);

  Future<T> requireService<T extends Service>(String name){
    var cpte = new Completer<T>();
    new Timer.periodic(const Duration(seconds: 1), (Timer timer){
      if (_host.controllers.containsKey(name)){
        if (_host.controllers[name].isServiceStarted){
          cpte.complete(_host.find(name));
          timer.cancel();
        }
      }
    });
    return cpte.future;
  }
}


class ServiceHost {
  bool _isStarted = false;
  Map<String,Service> services;
  Map<String,ServiceController> controllers;

  ServiceHost(this.services);

  void register(String name,Service service){
    if (!services.containsKey(name)){
      services[name] = service;
      if (_isStarted){
        _readyService(name, service);
      }
      return;
    }
    throw new Exception("Registering service name could not same as registered"); // TODO(thisLight): Use custom error class
  }

  Future startup(){
    var futures = <Future>[];
    services.forEach((name,service){
      futures.add(_readyService(name, service));
    });
    _isStarted = true;
    return Future.wait(futures);
  }

  Future _readyService(String name,Service service){
    var controller = new ServiceController(this,name,service);
    controllers[name] = controller;
    return service.init(controller);
  }

  T find<T extends Service>(String name){
    return controllers[name] as T;
  }
}
