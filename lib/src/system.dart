import "dart:async";
import './services.dart';


const SYS_SERV_ALARM = "system.alarm";
const SYS_SERV_MESSAGE = "system.message";
const SYS_SERV_JOB = "system.job";
const SYS_SERV_FLOWRUNNER = "system.flow_runner";


int _getNextToken(){
  return system.service<SystemMessageService>(SYS_SERV_MESSAGE).getNextToken();
}


final System system = new System();

class System {
  ServiceHost host;

  System() {
    _ready();
  }

  Future startup() async {
    await host.startup();
    new Ring('system.is_started', 0).send();
  }

  void _ready() {
    host = new ServiceHost({});
    host.register(SYS_SERV_ALARM, new SystemAlarmService());
    host.register(SYS_SERV_MESSAGE, new SystemMessageService());
    host.register(SYS_SERV_JOB, new SystemJobService());
    host.register(SYS_SERV_FLOWRUNNER, new SystemFlowRunnerService());
  }

  T service<T extends Service>(String name) => host.find(name);
}

void syspanic(Function f, String description) {
  throw new Exception(
      "System Panic in Runtime: $description on $f"); //TODO: use custom error
}

typedef void AlarmHandler();

abstract class Alarm {
  void onTime(AlarmHandler handler);
  void check(SystemAlarmService service);

  static OnTimeAlarm at(String name, DateTime time, {bool runOnce: false}) {
    return new OnTimeAlarm(name, time, isRunOnce: runOnce);
  }

  static RepeaticAlarm repeatic(String name, Duration duration,
      {int repeatTimes: -2}) {
    return new RepeaticAlarm(name, duration, repeatTimes: repeatTimes);
  }

  void setup() {
    var serv = system.service(SYS_SERV_ALARM) as SystemAlarmService;
    serv.alarms.add(this);
  }
}

class OnTimeAlarm implements Alarm {
  String name;
  DateTime time;
  bool isRunOnce;
  AlarmHandler handler;

  OnTimeAlarm(this.name, this.time, {this.isRunOnce: false});

  void onTime(AlarmHandler handler) {
    this.handler = handler;
  }

  void check(SystemAlarmService service) {
    var now = new DateTime.now();
    if (time.isAfter(now)) {
      handler();
      if (isRunOnce) {
        service.alarms.remove(this);
      } else {
        time = time.add(new Duration(days: 1));
      }
    }
  }

  void setup() => super.setup();

  String toString() =>
      "OnTimeAlarm($name,nextRunIn:$time,repeat?:${!isRunOnce})";
}

class RepeaticAlarm implements Alarm {
  String name;
  Duration duration;
  DateTime lastTimeRun;
  DateTime get nextTimeRun => lastTimeRun.add(duration);
  int repeatTimes;
  AlarmHandler handler;

  RepeaticAlarm(this.name, this.duration, {this.repeatTimes: -2}) {
    lastTimeRun = new DateTime.now();
  }

  void onTime(AlarmHandler handler) {
    this.handler = handler;
  }

  void check(SystemAlarmService service) {
    if (nextTimeRun.isBefore(new DateTime.now())) {
      handler();
      if (repeatTimes > -2) {
        repeatTimes--;
        if (repeatTimes == -1) service.alarms.remove(this);
        if (repeatTimes <= -2)
          syspanic(this.check, "system code logic goes wrost");
      }
    }
  }

  void setup() => super.setup();

  String toString() =>
      "RepeaticAlarm($name,$duration,lastTimeRun:$lastTimeRun,nextRunIn:$nextTimeRun,repeat?:${repeatTimes==-2?true:false},repeatTimes:$repeatTimes)";
}

class SystemAlarmService implements Service {
  // TODO: make alarm more effectient
  Timer timer;
  List<Alarm> alarms;

  @override
  Future init(ServiceController controller) async {
    timer = new Timer.periodic(const Duration(milliseconds: 200), _handleTimer);
    alarms = [];
  }

  void _handleTimer(Timer timer) {
    if (alarms.isNotEmpty) {
      alarms.forEach((Alarm alarm) => alarm.check(this));
    }
  }
}


typedef Future JobFunction(Job job);


class JobTemplate {
  JobFunction function;
  List<String> activeRings;
  List<String> activeSignals;

  JobTemplate(this.function){
    activeRings = [];
    activeSignals = [];
  }

  void activateByRing(String name) => activeRings.add(name);
  
  void activateBySignal(String name) => activeSignals.add(name);

  Job _createJob(Map<String,dynamic> arguments){
    var job = new Job(this, {});
    job.spaces.addAll(arguments);
    _setUpJob(job);
    return job;
  }

  void _setUpJob(Job job){
    activeRings.forEach((name) => new RingAccpeter(job.runWithRing).accept(name));
    activeSignals.forEach((name) => new SignalEntryPoint(name).then(job.runWithSignal));
    (system.service(SYS_SERV_JOB) as SystemJobService).register(job);
  }

  Job createJob(Map<String,dynamic> arguments){
    return _createJob(arguments);
  }

  Job runJob(Map<String,dynamic> arguments){
    var job = createJob(arguments);
    job.run({});
    return job;
  }
}


class Job {
  JobTemplate template;
  Map<String,dynamic> spaces;
  Completer _completer;
  int token;
  Future get future => _completer.future;

  Job(this.template,this.spaces){
    token = _getNextToken();
    _completer = new Completer();
  }

  Future run(Map<String,dynamic> arguments){
    spaces.addAll(arguments);
    template.function(this);
    return future;
  }

  Future runWithRing(Ring ring){
    return run({'_ring':ring});
  }

  Future runWithSignal(Signal sig){
    return run({'_signal':sig});
  }

  void done() => _completer.complete();
}


class SystemJobService implements Service {
  Map<int,Job> jobs;

  SystemJobService();

  @override
  Future init(ServiceController controller) {
    jobs = {};
    return new Future.value(null);
  }

  void register(Job job){
    jobs[job.token] = job;
  }
}

typedef void RingListener(Ring ring);

class Ring {
  static Map<String, List<RingListener>> _listeners = {};

  String target;
  int code;

  Ring(this.target, this.code);

  void send() {
    var llist = _getListeners(target) ?? const [];
    if (llist.isNotEmpty) {
      llist.forEach((listener) => listener(this));
    }
  }

  static List<RingListener> _getListeners(String target) {
    return _listeners[target];
  }

  static void addListener(String t, RingListener l) {
    if (_listeners[t] != []) {
      _listeners[t] = [];
    }
    _listeners[t].add(l);
  }

  static Ring qsend(String target, int code) {
    var ring = new Ring(target, code);
    ring.send();
    return ring;
  }
}

/// ````
/// new RingAccepter((ring) => print("${ring} recviced"))
///   ..accept("test.test")
///   ..accept("test.test2")
/// ````
class RingAccpeter {
  RingListener listener;

  RingAccpeter(this.listener);

  void accept(String target) {
    Ring.addListener(target, this);
  }

  void call(Ring ring) {
    listener(ring);
  }
}

typedef Future SignalListener(Signal signal);

class Signal<T> {
  String target;
  Map<String,dynamic> arguments;
  Completer<T> _completer;

  Signal(this.target,[this.arguments = const {}]){
    _completer = new Completer<T>();
  }

  dynamic operator[](String key) => arguments[key];
  void operator[]=(String key,dynamic value) => arguments[key] = value;

  Future<T> send(){
    system.service<SystemMessageService>(SYS_SERV_MESSAGE).signalManager.send(this);
    return future;
  }

  Future<T> get future => _completer.future;

  void complete(T value){
    _completer.complete();
  }

  void completeError(Object error,[StackTrace trace]){
    _completer.completeError(error,trace);
  }

  bool get isCompleted => _completer.isCompleted;

  static Signal qsend(String target, [Map<String,dynamic> arguments = const {}]){
    var sig = new Signal(target,arguments);
    sig.send();
    return sig;
  }
}

class SignalManager {
  static const SIGEV = "firefly.signal";
  Map<String, List<SignalListener>> listeners;
  SystemMessageService _service;

  SignalManager() {
    listeners = {};
    _service = system.service(SYS_SERV_MESSAGE) as SystemMessageService;
    _service.getEvents(SIGEV)
      .map((_Event<Signal> event) => event.value)
      .listen(_onSignal);
  }

  void _onSignal(Signal signal){
    (listeners[signal.target] ?? const <SignalListener>[]).forEach((l) => l(signal));
  }

  void register(String k, SignalListener l){
    if (!listeners.containsKey(k))
      listeners[k] = [];
    listeners[k].add(l);
  }

  void send(Signal sig){
    _service.eventbus.add(new _Event(SIGEV,sig));
  }
}


class SignalEntryPoint {
  String target;

  SignalEntryPoint(this.target);

  void then(SignalListener l) =>
    system.service<SystemMessageService>(SYS_SERV_MESSAGE).signalManager
      .register(target,l);
}


class Channel<T> {
  static const CHANEVADD = "firefly.channel.add";
  static const CHANEVSEND = "firefly.channel.send";
  static SystemMessageService _serv = system.service<SystemMessageService>(SYS_SERV_MESSAGE);

  String name;
  int token;
  StreamController<T> controller;
  Stream<T> get stream => controller.stream;

  Channel(this.name){
    token = _serv.getNextToken();
    controller = new StreamController<T>.broadcast();
    Signal.qsend(CHANEVADD,{
      'channel': this
    });
  }

  void send(T value){
    Signal.qsend(CHANEVSEND,{
      'channel': {
        'name': this.name,
        'token': this.token
      },
      'message': value
    });
  }
}

class _Event<T> {
  final String ev;
  final T value;

  _Event(this.ev, this.value);
}

/// How Firefly's Message Services Run?
/// Firefly Message Services include four parts: Ring, Signal, Channel, DataSink
///
/// Ring is a one-way notify method.
/// Signal is a two-way notify method, it allow called listener give a value back to sender of the signal
/// Channel is a two-way message queue, it allow many parts of code transfer values
/// DataSink is a single-way data channel, it allow one part send data to another part. // To be implemented, wait for while
class SystemMessageService implements Service {
  StreamController<_Event> eventbus;
  Stream<_Event> get events => eventbus.stream;

  Map<String,List<Channel>> channels;

  SignalManager signalManager;

  int _currTokenN = -1;

  SystemMessageService() {
    eventbus = new StreamController.broadcast();
    signalManager = new SignalManager();
    channels = {};
  }

  @override
  Future init(ServiceController controller) async {
    new SignalEntryPoint(Channel.CHANEVADD)
      .then(_onChannelAddRequest);
    new SignalEntryPoint(Channel.CHANEVSEND)
      .then(_onChannelSendRequest);
  }

  Future _onChannelAddRequest(Signal sig){
    var chan = sig['channel'] as Channel;
    if (!channels.containsKey(chan.name)){
      channels[chan.name] = [];
    }
    channels[chan.name].add(chan);
    return new Future.value();
  }

  Future _onChannelSendRequest(Signal sig){
    var chanName = sig['channel']['name'];
    var chanToken = sig['channel']['token'];
    var message = sig['message'];
    ((channels[chanName] ?? const []) as List<Channel>)
      .where((chan) => chan.token != chanToken)
      .forEach((chan) => chan.controller.add(message));
    return new Future.value();
  }

  Stream<_Event> getEvents(String ev) {
    return events.where((event) => event.ev == ev);
  }

  int getNextToken(){
    return ++_currTokenN;
  }
}


typedef Future FlowProcessor(FlowBoat boat);


class Flow {
  List<FlowProcessor> processors;

  Flow([this.processors]){
    processors = processors ?? [];
  }

  FlowBoat createBoat([Map<String,dynamic> arguments]){
    var boat = new FlowBoat(this,arguments);
    return boat;
  }

  Future<FlowBoat> run([Map<String,dynamic> arguments]){
    var boat = createBoat(arguments);
    return boat.start();
  }
}


class FlowBoat {
  Flow flow;
  int pointer;
  Map<String,dynamic> spaces;
  Completer<FlowBoat> completer;
  Future<FlowBoat> get future => completer.future;

  FlowBoat(this.flow,[this.spaces]){
    pointer = -1;
    spaces = spaces ?? {};
    completer = new Completer();
  }

  Future run(){
    ++pointer;
    var value = flow.processors[pointer](this);
    if (isDone()){
      completer.complete(this);
    }
    return value;
  }

  bool canRun(){
    return pointer < (flow.processors.length-1);
  }

  bool isDone(){
    return (pointer >= (flow.processors.length-1)) && (pointer != -1);
  }

  void start(){
    Signal.qsend(SystemFlowRunnerService.RUNCMD,{
      'boat': this
    });
  }
}


class SystemFlowRunnerService implements Service {
  static const RUNCMD = "system.flow_runner.run";
  List<FlowBoat> active;
  List<FlowBoat> inactive;
  bool _isWaken = false;

  @override
  Future init(ServiceController controller) {
    new SignalEntryPoint(RUNCMD)
        .then(_addBoat);
    return new Future.value(null);
  }

  Future _addBoat(Signal sig){
    inactive.add(sig['boat']);
    _awakeRunner();
    return new Future.value(null);
  }

  void _awakeRunner(){
    if (!_isWaken){
      _runnerHandling();
    }
  }

  void _runnerHandling(){
    _isWaken = true;
    if(active.length<=inactive.length) _exchangeActive();
    _genericCollect();
    _startFlows();
  }

  void _exchangeActive(){
    var tmp = active;
    active = inactive;
    inactive = tmp;
  }

  void _genericCollect(){
    inactive.removeWhere((boat) => boat.completer.isCompleted);
  }

  void _startFlows(){
    var futures = active.map((boat) => boat.run());
    Future.wait(futures)
      .then((_){
        if ((active.length+inactive.length)>0){
          scheduleMicrotask(_runnerHandling);
        } else {
          _turnRest();
        }
      });
  }

  void _turnRest(){
    _isWaken = false;
  }
}
