import 'package:firefly/test.dart';

class TestingTestCase extends TestCase {
  bool isSetUpAllRun = false;
  bool isTearDownAllRun = false;
  int setUpRunningCount = 0;
  int tearDownRunningCount = 0;

  void setUpAll(){
    isSetUpAllRun = true;
  }

  void testSetUpAllIsRun(){
    check(isSetUpAllRun,"setUpAll is not run");
  }

  void tearDownAll(){
    isTearDownAllRun = true;
    check(isTearDownAllRun, "tearDownAll is not run");
    check(setUpRunningCount == 3, "setUp run incorrectly");
    check(tearDownRunningCount == 3, "tearDown run incorrectly");
  }

  void testFailCanRun(){
    // When need to check check can fail
    // check(false,"must fail");
    // fail();
  }

  void testHasKeyRunCorrectly(){
    var testMap = {'key':true};
    checkHasKey(testMap, 'key',"must had 'key");
  }

  void setUp(){
    setUpRunningCount++;
  }

  void tearDown(){
    tearDownRunningCount++;
  }
}


main(){
  registerCase(TestingTestCase);
  runTest();
}
